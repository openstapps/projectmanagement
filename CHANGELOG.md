# [0.25.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.24.0...v0.25.0) (2023-01-17)


### Bug Fixes

* broken cypress build ([a37d3a9](https://gitlab.com/openstapps/projectmanagement/commit/a37d3a9ec4c8303bef7f7f29fdb8f7401ef7b02a))
* renovate job timeout ([116c880](https://gitlab.com/openstapps/projectmanagement/commit/116c8809e4f7d524361275e910fdc582a9baf78a))



# [0.24.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.23.1...v0.24.0) (2022-10-10)



## [0.23.1](https://gitlab.com/openstapps/projectmanagement/compare/v0.23.0...v0.23.1) (2022-07-05)


### Bug Fixes

* documentation not generating ([ac226d1](https://gitlab.com/openstapps/projectmanagement/commit/ac226d178a41e24a57f5e24409255aee2cfabf8d))



# [0.23.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.22.0...v0.23.0) (2022-07-05)



# [0.22.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.21.0...v0.22.0) (2021-12-14)



# [0.21.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.20.1...v0.21.0) (2021-12-14)


### Bug Fixes

* exclude npm audit from maintenance runs ([42e6812](https://gitlab.com/openstapps/projectmanagement/commit/42e68120b7215b6fe0f929955d99907103b7bfed))
* link to old gitlab server ([aa9991a](https://gitlab.com/openstapps/projectmanagement/commit/aa9991ad708dd80b873b738a304c94f6f21dd4ba)), closes [#27](https://gitlab.com/openstapps/projectmanagement/issues/27)


### Features

* make slack notification optional ([84921ad](https://gitlab.com/openstapps/projectmanagement/commit/84921ad5ce90c24ce53197235e8f8a8aff0f4aba))



## [0.20.1](https://gitlab.com/openstapps/projectmanagement/compare/v0.20.0...v0.20.1) (2020-07-27)


### Bug Fixes

* ignore archived projects in milestone tidy ([cd768a9](https://gitlab.com/openstapps/projectmanagement/commit/cd768a9dcdb8edc62f8277a45b054e261311e404))
* remove invalid project name element ([e4c065d](https://gitlab.com/openstapps/projectmanagement/commit/e4c065d5058512ecab1b7fb173e34d950abb123f))


### Features

* add merge request assignment ([65d05bf](https://gitlab.com/openstapps/projectmanagement/commit/65d05bfe692c7589ae4138439724ba7c0c73bec5))



# [0.20.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.19.0...v0.20.0) (2020-05-06)


### Features

* change builder base image to docker/compose ([5ea40b2](https://gitlab.com/openstapps/projectmanagement/commit/5ea40b2ece8cb2ae764096121492dd9703963f1d))



# [0.19.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.18.0...v0.19.0) (2020-01-08)



# [0.18.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.17.1...v0.18.0) (2019-11-25)



## [0.17.1](https://gitlab.com/openstapps/projectmanagement/compare/v0.17.0...v0.17.1) (2019-11-19)


### Features

* add documentation for automatic publishing ([ab45e35](https://gitlab.com/openstapps/projectmanagement/commit/ab45e35117ef0cca3f42c557e100d4e57a2571df)), closes [#23](https://gitlab.com/openstapps/projectmanagement/issues/23)



# [0.17.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.16.1...v0.17.0) (2019-11-13)


### Features

* update to new meeting date ([98c73b5](https://gitlab.com/openstapps/projectmanagement/commit/98c73b575846fb342c4ffb6a2cf09152a3205f95))



## [0.16.1](https://gitlab.com/openstapps/projectmanagement/compare/v0.16.0...v0.16.1) (2019-08-20)


### Bug Fixes

* add colors to cli log output ([7d271a7](https://gitlab.com/openstapps/projectmanagement/commit/7d271a73648d2367e57a3b6c6bb5725d106dced6))



# [0.16.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.15.0...v0.16.0) (2019-08-20)


### Features

* only unlabel closed issues before last meeting ([d7b68ae](https://gitlab.com/openstapps/projectmanagement/commit/d7b68ae45c8321f0a1d9cba998b604507445f9fb))



# [0.15.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.14.1...v0.15.0) (2019-07-23)


### Features

* add function and task to get version of used dependency ([067a201](https://gitlab.com/openstapps/projectmanagement/commit/067a2011c03b1e3e9d164844d594e4f332982f12)), closes [#20](https://gitlab.com/openstapps/projectmanagement/issues/20)



## [0.14.1](https://gitlab.com/openstapps/projectmanagement/compare/v0.14.0...v0.14.1) (2019-06-05)



# [0.14.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.13.0...v0.14.0) (2019-06-04)


### Bug Fixes

* fix regression with protected tags ([65a7233](https://gitlab.com/openstapps/projectmanagement/commit/65a723390572f48601e52dac975c9e2b4a0f06c4))


### Features

* add max depth for reminders ([3641ec4](https://gitlab.com/openstapps/projectmanagement/commit/3641ec4d00890f81b24301b61d689cd113e33997))



# [0.13.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.12.1...v0.13.0) (2019-06-03)


### Features

* add curl to Node.js image ([2ec80fb](https://gitlab.com/openstapps/projectmanagement/commit/2ec80fbb2f1256530e61a389a8444163c7aef8bd)), closes [#18](https://gitlab.com/openstapps/projectmanagement/issues/18)



## [0.12.1](https://gitlab.com/openstapps/projectmanagement/compare/v0.12.0...v0.12.1) (2019-05-27)



# [0.12.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.11.0...v0.12.0) (2019-05-27)


### Bug Fixes

* use user 'node' in Node.js image ([0c8aa72](https://gitlab.com/openstapps/projectmanagement/commit/0c8aa7257add9fd5997105f18b69211152fd8971)), closes [#17](https://gitlab.com/openstapps/projectmanagement/issues/17)


### Features

* add git to node image ([c0058f9](https://gitlab.com/openstapps/projectmanagement/commit/c0058f9c2918bae3e5fce58f11802174ea08084c)), closes [#16](https://gitlab.com/openstapps/projectmanagement/issues/16)
* notify merge request assignee instead of author ([d60625c](https://gitlab.com/openstapps/projectmanagement/commit/d60625c461c6fe54b7c60091050a31f7f77a958e))



# [0.11.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.10.0...v0.11.0) (2019-04-30)


### Features

* tidy sub groups ([de05b52](https://gitlab.com/openstapps/projectmanagement/commit/de05b52f24985a882dbacdf82cc1a7c3c4efbe88))



# [0.10.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.9.0...v0.10.0) (2019-04-17)


### Features

* add timestamp to reports ([ad99e45](https://gitlab.com/openstapps/projectmanagement/commit/ad99e45ee3de59de4b5fcca312cb077d90d73335))



# [0.9.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.8.1...v0.9.0) (2019-04-09)


### Features

* add check for protected tags to tidy task ([feb0c3b](https://gitlab.com/openstapps/projectmanagement/commit/feb0c3ba019b1fdfdc5912db86b20d9cc2dd650f)), closes [#14](https://gitlab.com/openstapps/projectmanagement/issues/14)



## [0.8.1](https://gitlab.com/openstapps/projectmanagement/compare/v0.8.0...v0.8.1) (2019-03-20)



# [0.8.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.7.0...v0.8.0) (2019-03-20)


### Features

* use mustache instead of tangular ([95f7521](https://gitlab.com/openstapps/projectmanagement/commit/95f7521dbddc13a6acb5835ae05dd5535cce067f))



# [0.7.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.6.0...v0.7.0) (2019-03-20)



# [0.6.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.5.0...v0.6.0) (2019-02-25)


### Features

* adjust reminder for unfixed merge requests ([e02939c](https://gitlab.com/openstapps/projectmanagement/commit/e02939c470ff9a63fb33c9754e06ea4dfbbd9b15))



# [0.5.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.4.1...v0.5.0) (2019-02-05)


### Features

* add new task to remind about open merge requests ([201ec09](https://gitlab.com/openstapps/projectmanagement/commit/201ec093b7ab676d5d7a84972c5d9e64664af7e0))



## [0.4.1](https://gitlab.com/openstapps/projectmanagement/compare/v0.4.0...v0.4.1) (2019-01-30)



# [0.4.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.3.0...v0.4.0) (2019-01-30)


### Features

* add slack reminder for open merge requests ([47e9775](https://gitlab.com/openstapps/projectmanagement/commit/47e977583bc036d48194d1e6bc9e6214dbbe6062))



# [0.3.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.2.2...v0.3.0) (2019-01-28)


### Features

* include sub groups in report generation ([040c666](https://gitlab.com/openstapps/projectmanagement/commit/040c666e747df35b773be1a0e30b7ba99f3b6598))



## [0.2.2](https://gitlab.com/openstapps/projectmanagement/compare/v0.2.1...v0.2.2) (2019-01-23)


### Bug Fixes

* correctly determine project for issue ([8f06fc3](https://gitlab.com/openstapps/projectmanagement/commit/8f06fc35347f06e8409e4190079323af91fc0a37))



## [0.2.1](https://gitlab.com/openstapps/projectmanagement/compare/v0.2.0...v0.2.1) (2019-01-17)


### Bug Fixes

* correctly add shebang line to cli ([f2933ef](https://gitlab.com/openstapps/projectmanagement/commit/f2933ef9e23e6fa92086800d791c9e4c6685fd4b))



# [0.2.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.1.1...v0.2.0) (2019-01-17)


### Bug Fixes

* correct link for projects ([a95ec14](https://gitlab.com/openstapps/projectmanagement/commit/a95ec14be8e169ee1e9f84e3ef98a9712babd302))
* do not add issues to report from backlog ([e902d70](https://gitlab.com/openstapps/projectmanagement/commit/e902d705bf66e07f70a9c34f9f13160c5db5982f))
* unlabel issues with label meeting only ([41a349e](https://gitlab.com/openstapps/projectmanagement/commit/41a349ead50db256916f5506829e587d1ecb4a43))



## [0.1.1](https://gitlab.com/openstapps/projectmanagement/compare/v0.1.0...v0.1.1) (2019-01-07)


### Bug Fixes

* remove failing deletion of labels ([ac8f2e1](https://gitlab.com/openstapps/projectmanagement/commit/ac8f2e10f965ef0e4950d8f294b6c34a26f4bad0))



# [0.1.0](https://gitlab.com/openstapps/projectmanagement/compare/v0.0.2...v0.1.0) (2018-12-13)



## [0.0.2](https://gitlab.com/openstapps/projectmanagement/compare/v0.0.1...v0.0.2) (2018-12-03)


### Features

* add issue template for features ([8ae1a51](https://gitlab.com/openstapps/projectmanagement/commit/8ae1a5166962c5d0b41b869c6021ddc456407022))
* add nyc configuration ([883ab9b](https://gitlab.com/openstapps/projectmanagement/commit/883ab9bfa55def9a0a8dd4ee5427c1eb9ad2960b))



## [0.0.1](https://gitlab.com/openstapps/projectmanagement/compare/19fd0f6e4cb8b44152333242aa17db84b2bd038a...v0.0.1) (2018-11-29)


### Features

* add projectmanagement and pack ([19fd0f6](https://gitlab.com/openstapps/projectmanagement/commit/19fd0f6e4cb8b44152333242aa17db84b2bd038a))



