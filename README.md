# @openstapps/projectmanagement

[![pipeline status](https://img.shields.io/gitlab/pipeline/openstapps/projectmanagement.svg?style=flat-square)](https://gitlab.com/openstapps/projectmanagement/commits/master) 
[![npm](https://img.shields.io/npm/v/@openstapps/projectmanagement.svg?style=flat-square)](https://npmjs.com/package/@openstapps/projectmanagement)
[![license)](https://img.shields.io/npm/l/@openstapps/projectmanagement.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://openstapps.gitlab.io/projectmanagement)

This repository contains documentation, project resources and cli tools to manage the `@openstapps` projects.

## Meetings

We have developer meetings via video conference on at least every first and third wednesday of the month.

An agenda is generated with the project management tool that contains all issues that have the label `meeting`.

## Integrating new schools

### Subgroup in GitLab

Every school gets their own subgroup on GitLab inside the group `@openstapps`.

The technical name is the license plate of the school. This can be found in the [list of license plates](project-docs/SCHOOL_IDENTIFIERS.md).

The official name of the school is used as the group's name and a matching logo is picked to act as the group's logo - this should be a square one because of the way that GitLab handles/shows logos. 

#### Permissions on the group

Every school should have one maintainer on the main group. This should be someone that is involved in the development.

Inside the subgroup every school can decide on its own how to handle permissions.

### Adjusting common projects

#### `gitlab-ci.yml` in `@openstapps/backend`

Every school has their own Docker image for the backend because every school needs other plugins and configuration. This is easily accomplished with a dedicated entry in the [`.gitlab-ci-yml`](https://gitlab.com/openstapps/backend/-/blob/master/.gitlab-ci.yml).

#### Add namespace for UUID generation

To be able to generate UUIDs for things that need to be indexed/added to the backend every schools needs a namespace in the [`@openstapps/api`](https://gitlab.com/openstapps/api). This namespace can be any random, valid UUIDv4.

#### Add subgroup's id to the scripts configuration

To use the project management tool for the new subgroup, the id of that group needs to be added to the configuration of the tool.

The groups id can be found in the settings of the group on GitLab.

## Project management tool

This is a small cli tool that helps to maintain the project(s).

```shell
openstapps-projectmanagement --help
```
