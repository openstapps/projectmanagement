# How to contribute

To contribute effectively to this or other Open StApps projects you should read the documentation in this repository first.

* What's the scope of one [project](../project-docs/workflow/PROJECT.md)?
* How to create [issues](../project-docs/workflow/ISSUES.md) in projects.
* How to [set up](../project-docs/SETUP.md) your machine?
* How to contribute code?
  * What's the [branching model](../project-docsworkflow/BRANCHING.md)?
  * How to write [code](../project-docs/workflow/CODING.md)?
  * How to create [merge requests](../project-docs/workflow/MERGING.md)?
  * How to write [documentation](../project-docs/workflow/DOCUMENTATION.md)?
  * How to [version](../project-docs/workflow/VERSIONING.md) the code?

## Further resources

* [Docker cheat sheet](../project-docs/DOCKER_CHEAT_SHEET.md)
