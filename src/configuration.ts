/*
 * Copyright (C) 2018-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Label} from '@openstapps/gitlab-api/lib/types';
import moment from 'moment';

/**
 * List of schools with their IDs
 */
export const SCHOOLS: {[school: string]: number} = {};

/**
 * ID OF openstapps main group
 */
const STAPPS_GROUP_ID = 4_088_298;

/**
 * List of group IDs to fetch issues for
 */
export const GROUPS: number[] = [
  STAPPS_GROUP_ID,
  ...Object.keys(SCHOOLS).map(school => {
    return SCHOOLS[school];
  }),
];

/**
 *
 */
export const LABEL_WEIGHTS: {[key: string]: number} = {
  bug: 1,
  critical: 2,
};

/**
 * List of labels to print bold in report
 */
export const BOLD_LABELS: string[] = ['bug', 'critical'];

/**
 * GitLab API URL
 */
export const GITLAB_API_URL = 'https://gitlab.com/api/v4/';

/**
 * Milestones to add to projects
 */
export const NEEDED_MILESTONES = ['Backlog'];

/**
 * Protected branches
 */
export const PROTECTED_BRANCHES = ['develop', 'master'];

/**
 * Labels to add to all projects
 */
export const NEEDED_LABELS: Label[] = [
  // eslint-disable-next-line unicorn/no-useless-spread
  ...[
    {
      color: '#FF0000',
      description: 'An error/something that is not working as expected',
      name: 'bug',
    },
    {
      color: '#5CB85C',
      name: 'consistency',
    },
    {
      color: '#FF0000',
      name: 'confirmed',
    },
    {
      color: '#FF0000',
      description: 'A blocking issue/something that needs to be fixed ASAP',
      name: 'critical',
    },
    {
      color: '#428BCA',
      name: 'design',
    },
    {
      color: '#0033CC',
      description: 'An issue about the documentation of the software',
      name: 'documentation',
    },
    {
      color: '#5CB85C',
      name: 'Doing',
    },
    {
      color: '#5CB85C',
      description: 'A feature proposal/something that will be developed',
      name: 'feature',
    },
    {
      color: '#7F8C8D',
      description: 'An issue that is unimportant or invalid',
      name: 'invalid',
    },
    {
      color: '#FFFF88',
      name: 'meeting',
    },
    {
      color: '#8E44AD',
      name: 'organization',
    },
    {
      color: '#FF0000',
      description: 'An issue with the performance of the software',
      name: 'performance',
    },
    {
      color: '#69D100',
      name: 'refactoring',
    },
    {
      color: '#FF0000',
      description: 'An issue with the security of the software',
      name: 'security',
    },
    {
      color: '#D1D100',
      description: 'An issue about the testing procedure of the software',
      name: 'testing',
    },
    {
      color: '#F0AD4E',
      name: 'To Do',
    },
    {
      color: '#A8D695',
      description: 'An issue with low priority',
      name: 'unimportant',
    },
    {
      color: '#D10069',
      description: 'An issue with the usability of the software',
      name: 'usability',
    },
    {
      color: '#428BCA',
      description: 'Feedback from the feedback-module of the app',
      name: 'user-feedback',
    },
  ],
  ...Object.keys(SCHOOLS).map(school => {
    return {
      color: '#F0AD4E',
      description: 'An issue that specifically applies to this school',
      name: `school-${school}`,
    };
  }),
  ...['android', 'iOS', 'web', 'node'].map(platform => {
    return {
      color: '#FFECDB',
      description: 'An issue that specifically applies to this platform',
      name: `platform::${platform}`,
    };
  }),
];

/**
 * Prefix for automatically created notes
 */
export const NOTE_PREFIX = '`openstapps/projectmanagement`';

/**
 * Slack channel to post messages to
 */
export const SLACK_CHANNEL = 'C762UG76Z';

/**
 * Concurrency for async pool
 */
export const CONCURRENCY = 3;

/**
 * Maximum depth for merge request reminders
 */
export const MAX_DEPTH_FOR_REMINDER = 2;

/**
 * Next meeting
 */
export const NEXT_MEETING = moment()
  .startOf('week')
  // tslint:disable-next-line:no-magic-numbers
  .hour(10)
  // tslint:disable-next-line:no-magic-numbers
  .day(3);

if (NEXT_MEETING.isBefore(moment())) {
  NEXT_MEETING.add(1, 'week');
}

/**
 * Last meeting
 */
export const LAST_MEETING = moment(NEXT_MEETING).subtract(1, 'week');
