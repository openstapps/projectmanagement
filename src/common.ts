/*
 * Copyright (C) 2018-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {asyncPool} from '@krlwlfrt/async-pool';
import {Api} from '@openstapps/gitlab-api';
import {Group, Project} from '@openstapps/gitlab-api/lib/types';
import {Logger} from '@openstapps/logger';
import {readFile, writeFile} from 'fs';
import {promisify} from 'util';
import {CONCURRENCY} from './configuration';

/**
 * Promisified version of readFile
 */
export const readFilePromisified = promisify(readFile);

/**
 * Promisified version of writeFile
 */
export const writeFilePromisified = promisify(writeFile);

/**
 * Get projects for a list of groups
 *
 * @param api GitLab API to make requests with
 * @param groups List of groups
 */
export async function getProjects(api: Api, groups: number[]): Promise<Project[]> {
  Logger.info(`Fetching all projects for specified groups (${groups.length})...`);

  const projectResults = await asyncPool(CONCURRENCY, groups, async groupId => {
    return api.getProjectsForGroup(groupId);
  });

  const projects = flatten2dArray(projectResults);

  Logger.log(`Fetched ${projects.length} project(s).`);

  return projects;
}

/**
 * Get subgroups for a list of groups
 *
 * @param api GitLab API to make requests with
 * @param groups List of groups
 */
export async function getSubGroups(api: Api, groups: number[]): Promise<Group[]> {
  return flatten2dArray(
    await asyncPool(CONCURRENCY, groups, async groupId => {
      return api.getSubGroupsForGroup(groupId);
    }),
  );
}

/**
 * Flatten 2d array
 *
 * @param array Flattened array
 */
export function flatten2dArray<T>(array: T[][]): T[] {
  // eslint-disable-next-line unicorn/prefer-spread
  return ([] as T[]).concat(...array);
}
