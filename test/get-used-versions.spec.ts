import * as chai from 'chai';
import {expect} from 'chai';
import chaiAsPromised from 'chai-as-promised';
import {execSync} from 'child_process';
import {suite, test} from '@testdeck/mocha';
import {join} from 'path';
import {dirSync} from 'tmp';
import {getUsedVersion, getUsedVersionMajorMinor} from '../src/tasks/get-used-version';

chai.use(chaiAsPromised);
chai.should();

@suite()
export class GetUsedVersionsSpec {
  @test
  async 'does not depend on core'() {
    return getUsedVersion(join(__dirname, '..'), '@openstapps/core').should.eventually.be.rejected;
  }

  @test
  async 'not a Node.js project'() {
    return getUsedVersion(__dirname, '@openstapps/core').should.eventually.be.rejected;
  }

  @test
  async 'has no dependencies'() {
    const temporaryDirectory = dirSync();

    execSync(`cd ${temporaryDirectory.name}; npm init -y`);

    await getUsedVersion(temporaryDirectory.name, '@openstapps/core').should.eventually.be.rejected;
  }

  @test
  async 'get used version'() {
    expect(await getUsedVersion(join(__dirname, '..'), '@krlwlfrt/async-pool')).to.be.equal('0.4.1');
  }

  @test
  async 'get used version major minor'() {
    expect(await getUsedVersionMajorMinor(join(__dirname, '..'), '@krlwlfrt/async-pool')).to.be.equal('0.4');
  }
}
