To identify schools we use their license plates.

| City | School | License- | plate |
| --- | --- | --- | --- |
| Aachen | KFH NRW | AC | KF |
| Aachen | Theolog. HS | AC | FK |
| Aachen | Fachhochschule | AC | FH |
| Aachen | MuHo | AC | HM |
| Aachen | Rheinisch-Westf. TH | AC | TH |
| Aachen-Jülich | FachHS | DN | FH |
| Aalen | Fachhochschule | AA | FH |
| Albstadt | HS Albst.-Sigma. | SIG | F2 |
| Alfter | Alanus HS | SU | AH |
| Altenholz | FH Verwaltung | RD | VF |
| Amberg FH | Abt. Weiden | AM | F2 |
| Amberg | FH Abt. Amberg | AM | FH |
| Ansbach | FH | AN | FH |
| Arnstadt | FH Kunst | IK | FH |
| Aschaffenburg | FH | AB | FH |
| Aschersleben | FH Polizei | ASL | VF |
| Augsburg | HS f. Musik | A | HM |
| Augsburg | Universität | A | U |
| Augsburg | Fachhochschule | A | FH |
| Bad Hersfeld | HS Unfallv. | HEF | FH |
| Bad Homburg | HS | HG | HS |
| Bad Honnef | Intern. FH | SU | IF |
| Bad Liebenzell | Int. HS | CW | IH |
| Bad Mergentheim | DualHS | TBB | DH |
| Bad Münstereifel | VerwFH | EU | VF |
| Bad Saarow | A. K. Akad. | LOS | PF |
| Bad Sooden-Allen. | DIPLFH | ESW | PF |
| Bamberg | Universität | BA | U |
| Bayreuth | HS f. Kirchenm. | BT | KH |
| Bayreuth | Universität | BT | U |
| Benediktbeuren | PhilTh. HS | GAP | KH |
| Benneckenstein | Verw. FH | WR | VF |
| Bergisch-Gladb. | Wirts. FH | PB | F2 |
| Berlin | Akkon HS (Pr. FH) | B | AH |
| Berlin | AMD | B | AM |
| Berlin | ASFH | B | FS |
| Berlin | bbw HS | B | AB |
| Berlin | BEST - Sabel FH | B | PF |
| Berlin | Beuth HS | B | TF |
| Berlin | Charite-Univ. Med. | B | MU |
| Berlin | DEKRA HS | B | DK |
| Berlin | Design Akademie | B | P2 |
| Berlin | EBC HS | B | BS |
| Berlin | EDU. CON HS | B | EH |
| Berlin | ESCP HS | B | HW |
| Berlin | ESMOD Kunst-HS | B | ES |
| Berlin | Euro. Sch. o. Mang. | B | EM |
| Berlin | Europ. College | B | EC |
| Berlin | Evang. HS | B | EF |
| Berlin | FH Bund / FB Ausw. | B | VF |
| Berlin | German Bus. Sch. | B | GB |
| Berlin | Hertie School | B | HS |
| Berlin | HS d. pop. Künste | B | PK |
| Berlin | HS f. Medien | B | MH |
| Berlin | HS f. Techn. u. W. | B | FT |
| Berlin | HS Gesdh. u. Sport | B | P3 |
| Berlin | HS Wirtsch. Recht | B | FB |
| Berlin | HS Wirtsch. T. K. | B | WT |
| Berlin | Humb. Uni Virch. K. | B | U2 |
| Berlin | Humboldt Uni | B | U |
| Berlin | HWR FB Verwaltung | B | V2 |
| Berlin | HWR FB Wirtschaft | B | FW |
| Berlin | IB Hochschule | B | P4 |
| Berlin | Internat. HS | B | IH |
| Berlin | kath. HS | B | KF |
| Berlin | Macromedia FH | B | MF |
| Berlin | Mediadesign HS | B | DH |
| Berlin | Psychoanal. Uni. | B | PU |
| Berlin | Psychol. HS | B | PH |
| Berlin | Quadriga HS | B | QH |
| Berlin | SRH-Hochschule | B | OT |
| Berlin | Steinbeis HS | B | FH |
| Berlin | Stenden Uni. | B | SU |
| Berlin | Techn. Kunst HS | B | TK |
| Berlin | Touro College | B | TC |
| Berlin | Uni d. Künste | B | HK |
| Berlin | Uni f. Weiterbld. | B | WB |
| Berlin | FH der DBP TELEKOM | B | FP |
| Berlin | FH VerwSozVers. | B | V3 |
| Berlin | Freie Universität | B | FU |
| Berlin | HS f. Schauspielk. | B | H3 |
| Berlin | HS f. Musik | B | HM |
| Berlin | Kirchliche HS | B | KH |
| Berlin | Kunsthochschule | B | H4 |
| Berlin | Tech. FH Lichtenb. | B | F3 |
| Berlin | Techn. FH | B | F5 |
| Berlin | Techn. FH Wedding | B | F2 |
| Berlin | Techn. Uni. | B | TU |
| Bernau | FH öff. Verw. BrBg. | BER | VF |
| Bernburg | HS Anhalt | BBG | FH |
| Bernburg | HS Landwirtsch. | BBG | HS |
| Bethel | Kirchliche HS | W | K2 |
| Bethel | Kirchl. HS (ev.) | BI | KH |
| Biberach a. d. Riss | FH | BC | FH |
| Bielefeld | FH d. Diakonie | BI | DU |
| Bielefeld | FH d. Mittels. | BI | FM |
| Bielefeld | FH Wirtsch. P. | PB | F4 |
| Bielefeld | Universität | BI | U |
| Bielefeld | FH | BI | FH |
| Bielefeld | FH Verw. | BI | VF |
| Bierbronnen | Priv. Wiss. HS | WM | HS |
| Bingen | FH | MZ | FH |
| Birkenfeld | FH Trier | TR | F2 |
| Bochum | EBZ Bus. School | BO | EB |
| Bochum | FH | BO | FH |
| Bochum | FH (alter StatS.) | BO | F1 |
| Bochum | FH Gesundheit | BO | FG |
| Bochum | Folkwang-HS | BO | HM |
| Bochum | TFH | BO | F3 |
| Bochum | Ev. FH Rhld-Westf-L | BO | KF |
| Bochum | Ruhruniversität | BO | U |
| Bonn | HS d. S-Finanzgrp. | BN | SG |
| Bonn | Priv. FH Berufstät. | BN | PF |
| Bonn | FH Bund Ausw. Angel. | BN | VF |
| Bonn | priv. FH Bibliothek. | BN | FH |
| Bonn | Rh. Friedr. Wilh.-Uni | BN | U |
| Bottrop | FH | BOT | FH |
| Brandenburg | Verw. FH | BRB | VF |
| Brandenburg | FH | BRB | FH |
| Braunschweig | FH | BS | FH |
| Braunschweig | HS Bi. Kün. | BS | HK |
| Braunschweig | TU Carolo-W. | BS | TU |
| Bremen | Apollon HS | HB | AH |
| Bremen | HIWL | HB | HI |
| Bremen | HS | HB | FH |
| Bremen | Jacobs University | HB | IU |
| Bremen | KunstHS | HB | HK |
| Bremen | Universität | HB | U |
| Bremen | VerwHS | HB | VF |
| Bremerhaven | HS | HB | H |
| Bruchsal | Priv. wiss. HS | KA | PU |
| Brühl | EU FH | BM | EU |
| Brühl | FH Bund | BM | FB |
| Buxtehude | Hochschule21 | STD | HS |
| Buxtehude | FH Hi/Holzm/Gö | STD | FH |
| Calw | FH f. Wirtsch. u. Med. | CW | FH |
| Calw | intern. Hochschule | CW | HS |
| Celle | Pr. FH Wirtsch. Hann | CE | PF |
| Chemnitz | TU | C | TU |
| Chemnitz-Breitenbrunn | TH | C | TH |
| Clausthal | TU | GS | TU |
| Coburg | Fachhochschule | CO | FH |
| Coburg | HS angew. Wissen. | CO | HS |
| Coburg-Münchberg | FH | CO | F2 |
| Cottbus | HS Lausitz | CB | FH |
| Cottbus | Brandenb. TU | CB | TU |
| Darmstadt | EHD | DA | KF |
| Darmstadt | FH Dieburg | DA | VF |
| Darmstadt | HS | DA | FH |
| Darmstadt | Wilhelm-B. HS | DA | PF |
| Darmstadt | TU | DA | TU |
| Darmstadt | VerwFH | DA | V2 |
| Deggendorf | FH | DEG | FH |
| Dessau | HS Anhalt | DE | FH |
| Detmold | FachHS Lippe | DT | FL |
| Detmold | FH OWL | LIP | H2 |
| Detmold | HS für Musik | LIP | HM |
| Detmold | HS für Musik/alt | DT | HM |
| Dieburg | Verwaltungs-FH | DA | V3 |
| Diepholz | priv. FH | DH | FH |
| Dortmund | FH | DO | FH |
| Dortmund | Folkw. HS Essen | DO | MH |
| Dortmund | ISM | DO | IS |
| Dortmund | Priv. FH Ökonom. | DO | PF |
| Dortmund | Universität | DO | U |
| Dortmund | MuHo Detmold | DO | HM |
| Dortmund | VerwFH | DO | VF |
| Dresden | Euro Business C. | DD | EC |
| Dresden | Ev. HS SozArbeit | DD | FH |
| Dresden | HS Kirchenmusik | DD | HS |
| Dresden | Palucca HS | DD | AK |
| Dresden | PH | DD | PH |
| Dresden | Priv. FH | DD | PF |
| Dresden | Priv. Uni | DD | PU |
| Dresden | Abt. Meissen HS L. | DD | HL |
| Dresden | FH TW. | DD | HT |
| Dresden | HS bild. Künste | DD | HK |
| Dresden | Med. Akad. | DD | MA |
| Dresden | MuHo | DD | HM |
| Dresden | PH Nossen | DD | P2 |
| Dresden | PH Radebeul | DD | P3 |
| Dresden | TU | DD | TU |
| Dresden | Verkehrsw. HS | DD | HV |
| Duesseldorf | Intern. Uni. | D | IU |
| Duisburg | Folkwang-HS | DU | HM |
| Duisburg | Priv. FH | DU | PF |
| Duisburg | Uni | DU | UG |
| Duisburg | FH öff. Verwalt. | DU | VF |
| Duisburg-Essen | Uni | DU | U |
| Düsseldorf | AMD | D | AM |
| Düsseldorf | EBC Priv. FH | D | EB |
| Düsseldorf | Fliedner FH | D | FF |
| Düsseldorf | Priv. FH Ökon. | D | PF |
| Düsseldorf | Fachhochsch. | D | FH |
| Düsseldorf | FH öff. Ver. | D | VF |
| Düsseldorf | Kunstakademie | D | HK |
| Düsseldorf | R.-Schum.-HS | D | HM |
| Düsseldorf | Universität | D | U |
| Eberswalde | HS | BAR | FH |
| Eberswalde | Fachhochschule | EW | FH |
| Eckernförde | FachHS Kiel | ECK | FH |
| Edenkoben | FinanzFH | LD | VF |
| Eichstätt | Kath. Univers. | EI | U |
| Elmshorn | FH | PI | F2 |
| Elsfleth | HS | OL | F2 |
| Elstal | HS dig. Medienpr. | HVL | HS |
| Elstal | Theol. Seminar | HVL | FH |
| Emden | HS | EMD | FH |
| Erding | FH angew. Manag. | ED | PF |
| Erfurt | Fachhochschule | EF | FH |
| Erfurt | Medizin. Akademie | EF | MH |
| Erfurt | Priv. FH Adam-Ries | EF | PF |
| Erfurt | Theol. Fak. | EF | KH |
| Erfurt | Uni | EF | U |
| Erfurt | Universität | EF | PH |
| Erlangen-Nürnberg | Uni | ER | U |
| Essen | FH | E | FH |
| Essen | FH f. Management | E | P2 |
| Essen | Priv. FH f. Berufst. | E | PF |
| Essen | Uni Klinikum | E | UK |
| Essen | Universit. (I) | E | U |
| Essen | Universität | E | UG |
| Essen | Folkwang-Hochschule | E | HM |
| Esslingen | Hochschule | ES | FT |
| Esslingen | FH Sozialwesen | ES | FS |
| Flensburg | FH | FL | FH |
| Flensburg | Uni | FL | PH |
| Flensburg | Universität | FL | U |
| Frankfurt | HfB | F | HB |
| Frankfurt | Prov. S. o. Mang. | F | MS |
| Frankfurt | Universität | F | U |
| Frankfurt | Bibl. FH | F | V2 |
| Frankfurt | FH | F | FH |
| Frankfurt | HS Bild. Küns. | F | HK |
| Frankfurt | MusKunstHS | F | HM |
| Frankfurt | Philos. Theol. HS | F | KH |
| Frankfurt | Provadis School | F | PF |
| Frankfurt | VerwFH | F | VF |
| Frankfurt/Oder | Europauni. | FF | EU |
| Freiberg | TU Bergakademie | FG | U |
| Freiburg | Ev. Hochschule | FR | FH |
| Freiburg | Kath. HS | FR | KF |
| Freiburg | Universität | FR | U |
| Freiburg | MuHo | FR | HM |
| Freiburg | PH | FR | PH |
| Friedberg | THM | FB | FH |
| Friedensau | Theolog. HS | HAL | KH |
| Friedrichshafen | DualHS | FN | DH |
| Friedrichshafen | Zepp. Uni | FN | U |
| Fulda | Fachhochschule | FD | FH |
| Fulda | Theolog. Fakultät | FD | KH |
| Fürstenfeld. | FH f. Verw. | FFB | VF |
| Furtwangen | HS | VS | FH |
| Geisenheim | HSRM | WI | F2 |
| Geislingen | FachHS Ulm | UL | F2 |
| Gelsenk. | FH Recklinghsn. | GE | F3 |
| Gelsenkirchen | FH | GE | FH |
| Gelsenkirchen | FH Bocholt | GE | F2 |
| Gelsenkirchen | VerwFH | GE | VF |
| Gera | SRH FH f. Gesundh. | G | PF |
| Germersheim | Uni Mainz | GER | U |
| Gießen | FTH | GI | FT |
| Gießen | THM | GI | FH |
| Gießen | Verwaltungs-FH | GI | VF |
| Gießen | J.-Liebig-Univer. | GI | U |
| Göppingen | techn. HS | GP | FH |
| Görlitz | FH | ZI | HW |
| Gotha | Verwaltungs FH | GTH | VF |
| Göttingen | FH im DRK | GÖ | HS |
| Göttingen | HS | GÖ | FH |
| Göttingen | G.-Aug.-Univ. | GÖ | U |
| Göttingen | PrivFH | GÖ | PF |
| Greifswald | Uni | GW | U |
| Greifswald | Uni Abt. NB. | NB | U |
| Greifswald | Uni m. Klinik. | GW | U2 |
| Greifswald | Uni o. Klinik. | GW | U1 |
| Greifswald | Uni | HGW | U |
| Güstrow | Baltic College | GÜ | FH |
| Güstrow | Verwaltungs-FH | GÜ | VF |
| Gütersloh | priv. FH d. Ws. | PB | F3 |
| Gütersloh | Priv. FH | GT | PF |
| Hachenburg | FH Dt. Bundesb. | WW | VF |
| Hagen | Fernuni | HA | UG |
| Hagen | FH Südwestfalen | HA | FH |
| Hagen | Märk. FH | HA | F2 |
| Hagen | VerwFH | HA | VF |
| Halberstadt | HS Harz | HBS | FH |
| Halberstadt | Verw. FH | HBS | VF |
| Halle | evang. HS f. Kirchm. | HAL | EH |
| Halle | Kunsthochschule | HAL | HK |
| Halle | PH | HAL | PH |
| Halle | Uni | HAL | U |
| Halle | Abt. Merseb. Uni | HAL | U2 |
| Halle-Köthen | Pädagog. HS | KÖT | PH |
| Halle-Wittenberg | Uni | HAL | UW |
| Hamburg | AMD | HH | AM |
| Hamburg | Bucerius Law S. | HH | BS |
| Hamburg | DFI Brand Acad. | HH | PF |
| Hamburg | EBC | HH | BC |
| Hamburg | Eur. Fernhochs. | HH | EF |
| Hamburg | FH | HH | FH |
| Hamburg | Hafencity Uni. | HH | HU |
| Hamburg | Helm. Schmidt-Uni | HH | UB |
| Hamburg | HFH | HH | FF |
| Hamburg | HS der Polizei | HH | VF |
| Hamburg | HS Fres. Idstein | HH | FI |
| Hamburg | HSBA School | HH | BA |
| Hamburg | Intern. BS | HH | IB |
| Hamburg | MFH München | HH | MF |
| Hamburg | MSH | HH | MS |
| Hamburg | Nordd. Akad. Fin. | HH | HF |
| Hamburg | Uni | HH | U |
| Hamburg | Uni f. Wirts. Pol. | HH | HS |
| Hamburg | Uni Klinikum | HH | UK |
| Hamburg | Ev. FH | HH | FS |
| Hamburg | HS Bild. Künste | HH | HK |
| Hamburg | MuHo | HH | HM |
| Hamburg-Harburg | TU | HH | TU |
| Hameln | HSW | HM | HS |
| Hamm | FH Hamm-Lippstadt | HAM | F2 |
| Hamm | Priv. FH | HAM | FH |
| Hannover | FH Gestaltg. | H | FG |
| Hannover | HS | H | FH |
| Hannover | HS f. Musik | H | HM |
| Hannover | Kommunale FH | H | VF |
| Hannover | Leibniz FH | H | LA |
| Hannover | Universität | H | U |
| Hannover | BiblInf. FH | H | FB |
| Hannover | Ev. FH | H | KF |
| Hannover | MedHo | H | MH |
| Hannover | MilMolk. FH | H | F3 |
| Hannover | PrivFH Wirtsch. | H | PF |
| Hannover | TiHo | H | TI |
| Heide | FH | HEI | FH |
| Heidelberg | HS für Musik | HD | HM |
| Heidelberg | Pädagog. HS | HD | PH |
| Heidelberg | Uni f. jüd. St. | HD | KH |
| Heidelberg | Universität | HD | U |
| Heidelberg | FH | HD | FH |
| Heidenheim | DualHS (BA) | HDH | DH |
| Heilbronn | Duale HS | HN | DH |
| Heilbronn | GermGrad. Sch. | HN | BS |
| Heilbronn | FH | HN | FH |
| Heilbronn-Künzelsau | FH | HN | F2 |
| Hennef | FH Bonn-Rhein-S. | SU | F3 |
| Hennef | Phil.-Theolog. HS | SU | K2 |
| Herford | HS Kirchenmusik | HE | HK |
| Herrsching | FH f. Verw. | STA | V2 |
| Hildesheim | FH Gestaltg. | HI | FG |
| Hildesheim | FH SozPäd. | HI | FS |
| Hildesheim | HS | HI | FH |
| Hildesheim | Nordd. HS | HI | V2 |
| Hildesheim | Priv. Komm. FH | HI | VF |
| Hildesheim | Uni | HI | U |
| Hildesheim | PolizeiFH | HI | V3 |
| Hof | FH | HO | FH |
| Hof | FH Abtlg. Münchberg | HO | F2 |
| Hof | FH f. Verw. | HO | VF |
| Hohenheim | Universität | S | U2 |
| Höhr-Grenzhs. | FH Koblenz | WW | FH |
| Holzminden | HS | HOL | FH |
| Horb | DualHS (BA) | FDS | DH |
| Höxter | FH Lippe | HX | FH |
| Höxter | FH OWL | LIP | H3 |
| Höxter | Uni Paderborn | HX | UG |
| Idar-Oberstein | FH Trier | TR | F3 |
| Idar-Oberstein | FH Mainz I | MZ | F5 |
| Idstein | FH Fresenius | WI | F5 |
| Idstein | FH Wiesbaden | WI | F4 |
| Idstein | HS Fresenius | RÜD | PF |
| Ilmenau | TU | IK | TU |
| Ilmenau | Universität | IL | TH |
| Ingolstadt | FH | IN | FH |
| Ingolstadt | kath. Uni Eich. | IN | U |
| Iserlohn | Bus. a. IT School | MK | TS |
| Iserlohn | FH Südwestfalen | MK | FH |
| Isny | Priv. FH | RV | PF |
| Jena | Fachhochschule | J | FH |
| Jena | Universität | J | U |
| Kaiserslautern | FH KL | KL | FH |
| Kaiserslautern | Techn. U. | KL | U |
| Kamp-Lintford | FH | WES | FH |
| Karlsruhe | DualHS (BA) | KA | DH |
| Karlsruhe | HS | KA | FH |
| Karlsruhe | Intern. Univ. | KA | PF |
| Karlsruhe | KIT | KA | U |
| Karlsruhe | AkBildKünste | KA | HK |
| Karlsruhe | Gestalt. HS | KA | HG |
| Karlsruhe | MuHo | KA | HM |
| Karlsruhe | PH | KA | PH |
| Kassel | CVJM-HS | KS | CV |
| Kassel | DIPLOMA FH | KS | PF |
| Kassel | FH Bund FB LS | KS | FH |
| Kassel | KIMS | KS | MS |
| Kassel | Uni (KunstHS) | KS | U3 |
| Kassel | Uni (ohn. KunstHS) | KS | UG |
| Kassel | VerwFH | KS | VF |
| Kassel-Witzenhsn. | Uni | KS | U2 |
| Kehl | FH öff. Verwaltung | OG | VF |
| Kempten | FH | KE | FH |
| Kiel | Chr.-Albr.-Univers. | KI | U |
| Kiel | FH | KI | FH |
| Kiel | FH Kunst u. Gestalt. | KI | F2 |
| Kiel | FH Machinenwesen | KI | F6 |
| Kiel | Muthesius KunstHS | KI | HK |
| Kiel | FH FB Sozialwesen | KI | F3 |
| Kiel | FH FB Technik | KI | F4 |
| Kiel | FH FB Wirtschaft | KI | F5 |
| Kiel | Pädagog. Hochschule | KI | PH |
| Kleve | FH Rhein-Waal | KLE | FH |
| Koblenz | FH | KO | FH |
| Koblenz | HS f. UnternFhrg. | KO | U3 |
| Koblenz | Uni Kobl.-Landau | KO | U |
| Koblenz | FH Verw.-Polizei | KO | VF |
| Koeln | Intern. Univers. | K | IU |
| Köln | CBS (Priv. FH) | K | CB |
| Köln | FH Mittelstand | K | FM |
| Köln | HS Fresenius Idst. | K | FP |
| Köln | KFH NRW | K | KF |
| Köln | Macromedia FH | K | MF |
| Köln | Priv. FH f. Ökonomie | K | P2 |
| Köln | Uni | K | U |
| Köln | Bibl. FH | K | FB |
| Köln | Dt. SportHS | K | U2 |
| Köln | FH | K | FH |
| Köln | FH Bund- inn. Verw. | K | VF |
| Köln | MedienKunstHS | K | HK |
| Köln | MuHo | K | HM |
| Köln | priv. FH | K | PF |
| Köln | VerwFH | K | V2 |
| Köln-Gummersbach | FH | K | F2 |
| Königswusterhausen | FH | KW | FH |
| Konstanz | FH | KN | FH |
| Konstanz | Priv. Kunst FH | KN | F2 |
| Konstanz | Uni | KN | U |
| Köthen | HS Harz | KÖT | FH |
| Köthen | TH | KÖT | TH |
| Köthen | Uni Halle | KÖT | U |
| Krefeld | HS Niederrhein | KR | FH |
| Lahr | Wissensch. HS | OG | FW |
| Landau | Uni Koblenz-Landau | LD | U |
| Landshut | FH | LA | FH |
| Langen | FH d. Bundes | DA | V4 |
| Leer | HS | LER | FH |
| Leipzig | Ak. D. Privat-HS | L | FB |
| Leipzig | FH DTE | L | F2 |
| Leipzig | Ev. FH | L | KH |
| Leipzig | Graphik. FH | L | H3 |
| Leipzig | HandelsHS | L | HH |
| Leipzig | MuHo | L | HM |
| Leipzig | MuHo (I) | L | H2 |
| Leipzig | PH | L | PH |
| Leipzig | priv. HandelsHS | L | HS |
| Leipzig | Techn. FH | L | HT |
| Leipzig | TH | L | TH |
| Leipzig | Uni | L | U |
| Leuna-Merseburg | Techn. HS | MER | TH |
| Leverk. | Pr. FH Ökonomie | LEV | PF |
| Leverkusen | FH Köln | LEV | FH |
| Lichtenwalde | Spark. FH | FG | FH |
| Lippe | FH Detmold | LIP | F2 |
| Lippe | FH OWL | LIP | H1 |
| Lippe-Lemgo | FH Lemgo | LIP | FH |
| Lippstadt | FH Hamm-Lipps. | SO | F2 |
| Lörrach | DualHS (BA) | LÖ | DH |
| Lübeck | FH | HL | FH |
| Lübeck | FH Bund | HL | VF |
| Lübeck | FH FB Technik | HL | F3 |
| Lübeck | Uni Klinikum | HL | UK |
| Lübeck | Universität | HL | U |
| Lübeck | FH FB Angew. Nat. | HL | F2 |
| Lübeck | MuHo | HL | HM |
| Ludwigsbgur | Hochschule | LB | V2 |
| Ludwigsburg | ev. HS | LB | FH |
| Ludwigsburg | PH | LB | PH |
| Ludwigsburg | PH Abt. Rtl. | LB | P2 |
| Ludwigsburg | FH Finanzen | LB | VF |
| Ludwigshafen | FH | LU | FH |
| Ludwigshafen | Ev. FachHS | LU | KF |
| Lüneburg | Uni | LG | U |
| Lüneburg | Uni Lg. Abt. Lg. FH | LG | FH |
| Magdeburg | Fachhochschule | MD | F2 |
| Magdeburg | FH | MD | FH |
| Magdeburg | TU | MD | TU |
| Magdeburg | HS Magdeb. Std. | MD | F3 |
| Magdeburg | MedAkad. | MD | MH |
| Magdeburg | PH | MD | PH |
| Magdeburg | Uni | MD | U |
| Mainz | FH | MZ | F4 |
| Mainz | Uni Abt. Koblenz-L. | MZ | U2 |
| Mainz | FH Bund Eisenbahnw. | MZ | VF |
| Mainz | FH Rh.-Pf. Mainz I | MZ | F3 |
| Mainz | FH Rh.-Pf. Mainz II | MZ | F2 |
| Mainz | Joh.-Gutenberg-Uni. | MZ | U |
| Mainz | Kath. FH | MZ | KF |
| Mannheim | DualHS (BA) | MA | DH |
| Mannheim | FachHS Gestalt. | MA | FG |
| Mannheim | Hochschule | MA | FT |
| Mannheim | HS d. Bundesag. | MA | BA |
| Mannheim | ArbeitsvewFH | MA | V3 |
| Mannheim | BundesverwFH | MA | V2 |
| Mannheim | HS Musik u. Kunst | MA | HM |
| Mannheim | Soz. FH | MA | FS |
| Mannheim | Uni | MA | U |
| Marburg | Evangelische HS | MR | EH |
| Marburg | Archivw. FH | MR | FH |
| Marburg | Uni | MR | U |
| Marl | Priv. FH Ökonomie | RE | PF |
| Mayen | FH inn. Verwaltung | MYK | VF |
| Meiningen | PolizeiVerwFH | MGN | VF |
| Meißen | FH Sächs. Verw. | MEI | FH |
| Meissen | FH Sächs. Verw. | MEI | FV |
| Meissen | VerwHS FB Finanz | MEI | F2 |
| Merseburg | HS | SK | HS |
| Merseburg | HS (I) | HAL | FH |
| Meschede | FH Südwestfalen | HSK | FH |
| Mettmann | Priv. FH | ME | FH |
| Metzingen | Priv. FH Kunst | RT | HS |
| Minden | FH Bielefeld | MI | FH |
| Mittweida | FH f. Technik | C | FH |
| Mittweida | HS | MW | HT |
| Mittweida | Ing. HS | HC | IH |
| Mittweida | HS f. Techn. Wir. | MTW | HT |
| Mönchenglad. | HS Niederr. | MG | FH |
| Moritzberg | FH f. Relig. | Z | FH |
| Mosbach | DualHS (BA) | MOS | DH |
| Mülheim | FH | MH | FH |
| München | AMD Hamburg | M | AM |
| München | Bund. Uni FH-STG | M | B2 |
| München | Bund. Uni Uni-STG | M | BW |
| München | Business School | M | EA |
| München | FH f. Verw. | M | VF |
| München | HS angew. Sprache | M | AS |
| München | Macrom. FH d. Med. | M | MF |
| München | AkadBildKünste | M | HK |
| München | FernsFilmHS | M | HS |
| München | FH | M | FH |
| München | KathStiftFH | M | KF |
| München | MuHo | M | HM |
| München | PhilHS | M | KH |
| München | PolitHS | M | HP |
| München | Priv. FH Fresen. | M | FP |
| München | TU | M | TU |
| München | Uni | M | U |
| München-Benedi. | Kath. FH | GAP | KS |
| München-Eichs. | kath. Uni | M | U2 |
| München-Weihenstephan | TU | M | T2 |
| Münster | Dt. HS d. Polizei | MS | PH |
| Münster | KFH NRW | MS | KF |
| Münster | Universität | MS | U |
| Münster | FH | MS | FH |
| Münster | FinanzVerwFH | MS | VF |
| Münster | KunstAkad. | MS | HK |
| Münster | MuHo Detmold | MS | HM |
| Münster | Phil. Theol. HS | MS | KH |
| Münster | VerwFH | MS | V2 |
| Naumburg | Kirchl. HS (ev) | NMB | KH |
| Neubrandenburg | HS | NB | FH |
| Neuendettelsau | Augustana | M | AH |
| Neuendettelsau | August. HS | M | K3 |
| Neuss | EUFH | NE | EU |
| Neuss | HS int. Wirtsch. | NE | HS |
| Neuss | Priv. FH | NE | PF |
| Neu-Ulm | FH | NU | FH |
| Nienburg | FH Hannover | H | F2 |
| Nordhausen | FH | NDH | FH |
| Nordkirchen | FH Finanzen | COE | VF |
| Nürnberg | Evang. HS | N | KF |
| Nürnberg | HS f. Musik | N | HM |
| Nürnberg | AkadBildKünste | N | HK |
| Nürnberg | Ev. FH | N | K1 |
| Nürnberg | Ev. FH Abt. Münch. | N | K2 |
| Nürnberg | Ev. FH Abt. Neuen. | N | K3 |
| Nürnberg | FH | N | FH |
| Nürnberg | Uni Erlangen-N. | N | U |
| Nürtingen | Landespf. FH | NT | FH |
| Nürtingen | PrivKunstFH | NT | F3 |
| Nürtingen-Geislingen | FH | GP | F2 |
| Oberlausitz | HS f. ev. KiMu | GR | KH |
| Oberursel | Ev. HS | HG | KH |
| Oestrich-Winkel | Europ. B. S | WI | EB |
| Offenbach | HS Gestaltung | OF | HK |
| Offenburg | FH | FR | F2 |
| Oldenburg | HS | OL | FH |
| Oldenburg | priv. FH | OL | PF |
| Oldenburg | Uni | OL | U |
| Osnabrück | FH / Haste | OS | F2 |
| Osnabrück | HS | OS | FH |
| Osnabrück | HS in Lingen | OS | F3 |
| Osnabrück | Universität | OS | U |
| Osnabrück | Kath. FH Nord. | OS | KF |
| Ottersberg | FH | VER | FH |
| Paderborn | FH f. Wirtsch. | PB | FH |
| Paderborn | KFH NRW | PB | KF |
| Paderborn | Theol. Fakult. | PB | KH |
| Paderborn | Uni | PB | UG |
| Paderborn | Wirts. FH | PB | F1 |
| Paderborn-Meschede | Uni. GH | PB | U2 |
| Passau | Universität | PA | U |
| Pforzheim | FH Gestaltung | PF | FG |
| Pforzheim | FH Wirtschaft | PF | FH |
| Pirmasens | FH KL | PS | FH |
| Plauen | Priv. FH | PL | FH |
| Potsdam | Business School | P | BS |
| Potsdam | FH | P | F2 |
| Potsdam | FH f. Sport | P | SH |
| Potsdam | Uni. of Mang. Com. | P | PF |
| Potsdam | Universität | P | U |
| Potsdam | FH | P | FH |
| Potsdam-Bab. | HS Film/Fer. | P | HF |
| Potsdam-Brandenbg. | FH | BRB | F2 |
| Radebeul | FH d. Verwaltung | DD | F2 |
| Ravensb.-Weing. | FH | RV | FH |
| Ravensburg | DualHS (BA) | RV | DH |
| Regensburg | HS kath. KM. | R | KH |
| Regensburg | Universität | R | U |
| Regensburg | Fachhochschule | R | FH |
| Reinfeld | FH Verw. | RD | V2 |
| Remagen | FH Koblenz | KO | F2 |
| Rendsburg / Pinneberg | HS | RD | BF |
| Rendsburg | FachHS Kiel | RD | FH |
| Reutlingen | ev. FH | RT | FH |
| Reutlingen | Theol. Seminar | RT | TS |
| Reutlingen | FH Techn./Wir. | RT | FT |
| Rheinbach | FH Bonn-Rh. S. | SU | F2 |
| Rheine | Mathias HS | ST | MH |
| Riedlingen | Fernhoschule | BC | F2 |
| Riesa | FH Sächs. Verw. | RIE | FH |
| Rinteln | FH Verwaltung | SHG | VF |
| Rosenheim | FH | RO | FH |
| Rostock | HM | ROS | HM |
| Rostock | Institut f. Musik | ROS | IM |
| Rostock | Priv. Hanseuniv. | HRO | HU |
| Rostock | Universität | HRO | U |
| Rostock | HS f. Musik u. Th. | HRO | HM |
| Rotenburg | VerwFH | ROF | VF |
| Rothenburg | FH Polizei | NOL | FH |
| Rottenburg | FH Forstwirt. | TÜ | FH |
| Rüsselsheim | HSRM | WI | F3 |
| Saarbrücken | FH Bergbau | SB | F2 |
| Saarbrücken | HS f. Musik | SB | HM |
| Saarbrücken | HS f. Präv. | SB | HS |
| Saarbrücken | FH Verwalt. | SB | VF |
| Saarbrücken | HS Bil. Kü. | SB | HK |
| Saarbrücken | HS Tech./Wi. | SB | FH |
| Saarbrücken | Kath. FH So. | SB | KF |
| Saarbrücken | Uni Saarland | SB | U |
| Salzgitter | HS BS-Wb. | SZ | FH |
| Schmalkalden | Fachhochsch. | SM | FH |
| Schwäbisch Gm. | FH Gest. | AA | FG |
| Schwäbisch Gmünd | PH | AA | PH |
| Schwäbisch Hall | HS Techn. | SHA | HT |
| Schwäbisch Hall | priv. FH | SHA | KH |
| Schwandorf | Priv. FH | SAD | PF |
| Schwarzburg | FH f. Forstw. | RU | FH |
| Schweinfurt | FH Würzb.-S. | WÜ | F2 |
| Schwerin | Baltic College | LDS | BC |
| Schwerin | Inst. f. Musik | SN | HM |
| Schwetzingen | FH Rechtspf. | HD | VF |
| Senftenberg | HS Lausitz | SFB | FH |
| Siegen | Priv. FH | SI | PF |
| Siegen | Uni | SI | UG |
| Sigmaringen | HS Albst.-Si. | SIG | FH |
| Soest | FH Südwestfalen | SO | FH |
| Soest | FH f. oeffentl. Verw. | SO | VF |
| Soest | UniGHS Paderborn | SO | UG |
| Speyer | HS f. Verwaltungsw. | SP | U |
| St. Augustin | FH Bonn-Rh. S. | SU | FH |
| St. Augustin | Phil.-TheolHS | SU | KH |
| Starnberg | FH f. Verw. | STA | VF |
| Steinfurt | FachHS Münster | ST | FH |
| Stendal | FH Altmark | SDL | FH |
| Stralsund | FH | HST | FH |
| Stralsund | FH Abt. Heilig. | HST | F2 |
| Straubing | FH Weihenst. | SR | FH |
| Stuttgart | AKAD FH | OG | FH |
| Stuttgart | Duale HS Präs. | S | DP |
| Stuttgart | DualHS (BA) | S | DH |
| Stuttgart | FH d. Medien | S | F2 |
| Stuttgart | Inst. of. Manag. | S | IM |
| Stuttgart | Kath. HS | S | KF |
| Stuttgart | Uni | S | U |
| Stuttgart | VerwAkad. DuHS | S | VA |
| Stuttgart | Waldorfpäd. HS | S | WH |
| Stuttgart | Ak. Bild. Kün. | S | HK |
| Stuttgart | FachHS Technik | S | FH |
| Stuttgart | FH Bibliothekw. | S | FB |
| Stuttgart | HS Musik/Kunst | S | HM |
| Stuttgart | Macromedia FH | S | MF |
| Stuttgart | priv. FH f. Gest. | S | F3 |
| Stuttgart | Uni | S | TH |
| Suderburg | HS BS-Wb. | UE | FH |
| Swisttal | FH Bund | K | V3 |
| Swisttal | FH Bund (I) | K | F3 |
| Trier | FH | TR | FH |
| Trier | Theolog. Fakultät | TR | KH |
| Trier | Universität | TR | U |
| Triesdorf | FH Weihensteph. | AN | F2 |
| Trossingen | HS für Musik | TUT | HM |
| Tübingen | Eb.-Karls-Univ. | TÜ | U |
| Tuttlingen | HS | TUT | F3 |
| Ulm | FH für Technik | UL | FH |
| Ulm | FH Kempten Abt. Ulm | UL | F4 |
| Ulm | Universität | UL | U |
| Vallendar | Theolog. HS | MYK | KH |
| Vechta | priv. FH | VEC | FH |
| Vechta | Univ. | VEC | U |
| Vechta | Kath. FH Norddtld. | VEC | KF |
| Verlbert | FH Bochum | BO | F2 |
| Villingen | FH für Polizei | VS | VF |
| Villingen-Schw. | DualHS | VS | DH |
| Villingen-Schw. | HS V-S. | VS | F2 |
| Warnemünde-Wustrow | HS See | ROS | HS |
| Wasserburg | FH f. Verw. | RO | VF |
| Wedel | PrivFH | PI | FH |
| Weihenstephan | FH | FS | FH |
| Weimar | HS für Musik | WE | HM |
| Weimar | Universität | WE | U |
| Weimar | Verw. FH | WE | VF |
| Weimar | HS Archit./Bauwes. | WE | TH |
| Weingarten | Pädagog. HS | KN | PH |
| Weingarten | PH | RV | PH |
| Weißenfels | HS f. GesWes. | WSF | PF |
| Wernigerode | HS Harz | WR | FH |
| Wernigerode | Verw. FH | WR | V2 |
| Wernigerode | VerwFH | QLB | VF |
| Wetzlar | THM | LDK | FH |
| Wiesbaden | EBS U Wirtsch. | WI | BS |
| Wiesbaden | FH Bund | WI | V1 |
| Wiesbaden | HSRM | WI | FH |
| Wiesbaden | VerwFH | WI | VF |
| Wildau | THS | KW | TH |
| Wilhelmshaven | HS | WHV | FH |
| Wismar | FH | HWI | FH |
| Wismar | TH | WIS | TH |
| Wismar-Warnemünde | FH | WIS | FH |
| Witten-Herd. | Priv. Uni | EN | U |
| Wolfenbüttel | HS BS-Wb. | WF | FH |
| Wolfsburg | HS BS-Wb. | WOB | FH |
| Worms | FH | WO | FH |
| Wuppertal | HS Musik Köln | W | HM |
| Wuppertal | Kirchliche HS | W | KH |
| Wuppertal | Uni | W | UG |
| Wuppertal | FH öff. Verw. | W | VF |
| Würzburg | HS für Musik | WÜ | HM |
| Würzburg | J.-Max.-Uni. | WÜ | U |
| Würzburg | FH Würzb-Schw. | WÜ | FH |
| Würzburg-Aschaffenbg. | FH | WÜ | F3 |
| Zittau | FH | ZI | HT |
| Zittau | Intern. HS Instit. | ZI | IH |
| Zittau | TH | ZI | TH |
| Zweibrücken | FH KL | ZW | FH |
| Zwickau TH | Abt. Glauchau | Z | T3 |
| Zwickau TH | Abt. Plauen | Z | T2 |
| Zwickau | HS Fresenius | Z | PF |
| Zwickau | HS Schneeberg | Z | HS |
| Zwickau | PH | Z | PH |
| Zwickau | TH | Z | TH |
| Zwickau | TU | Z | TU |
| Zwickau | Westsächs. HS | Z | HT |
| Zwickau | Wests. HS(Reichb.) | RC | HS |
| Zwickau-Auerbach | PH | Z | P2 |
