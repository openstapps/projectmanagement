# Docker cheat sheet

## List running containers

```shell
docker ps
```

## List running containers of a `docker-compose.yml`

In the directory of the `docker-compose.yml`:

```shell
docker-compose ps
```

## Restart container

`$CONTAINERID` from `docker ps`.

```shell
docker restart $CONTAINERID
```

## Run program in container

`$PROGRAM` can be any program that is installed in the container `bash`, `sh`, `node`, `curl`...

```shell
docker exec -it $CONTAINERID $PROGRAM
```

## Tidy system

This removes unused images, networks, ... from your system.

```shell
docker system prune
```

Please consult the [Docker documentation](https://docs.docker.com/engine/reference/commandline/system_prune/) for more information.

## Further resources

* [An Exhaustive Guide to Writing Dockerfiles for Node.js Web Apps](https://blog.hasura.io/an-exhaustive-guide-to-writing-dockerfiles-for-node-js-web-apps-bbee6bd2f3c4)
* [Docker and Node.js Best Practices
](https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md)
