# Branching model

## Possible branch names

* `master`
* `develop`
* `$ID-$TITLE`

### `master`

Current productive version of a project resides on the `master` branch.

### `develop`

The upcoming productive version - current development version - resides on the `develop` branch. The `develop` branch is unnecessary in smaller projects when the development is not too fast.

The `master` branch needs to be updated with a fast forward merge after every release in projects that have a `develop` branch.

### `$ID-$TITLE`

Development of features, bugfixes or refactoring is done in branches referencing an issue that follow the naming convention `$ID-$TITLE` where `$ID` is the number of the issue and `$TITLE` is the sluggified title of the issue. The [issues](ISSUES.md) integration of GitLab has a feature that creates such a branch and an accompanying merge request.

## Further resources

* [Git - A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)
