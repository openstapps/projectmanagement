# Project

A project is an entity that solves a specific task.

Abstractable components of multiple projects should be extracted into another new project.

Node.js projects are published on NPMjs.com in the `@openstapps` group.

## Structure

Node.js/TypeScript based projects should employ the following structure:

```
src/                  Sources
test/                 Test files
lib/                  Compiled sources
  cli.js              cli tool
  index.js            API (classes, function, constants, ...)
  index.d.ts          TypeScript definitions for API
.editorconfig
.gitignore            List of files that should be ignored by git
.npmignore            List of files that should be ignored by npm
package.json          Package description
package-lock.json     Meta file, that fixates dependencies
README.md             Description of project, entry point for documentation
tsconfig.json         TypeScript configuration
tslint.json           TSLint configuration
```

### `@openstapps/configuration`

For the contents and purpose of `.editorconfig`, `tsconfig.json` and `tslint.json` refer to `@openstapps/configuration`.

## Further resources

* [Coding style](CODING.md)
* [Versioning](VERSIONING.md)
