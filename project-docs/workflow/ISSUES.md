# Issues

Regardless of scope - bug, feature, discussion - technical or organisational - everything is an issue.

Issues keep a discussion of the topic, are related to a [branch](BRANCHING.md), keep track of the progress, link to related issues and streamline the process of development.

## Further resources

* [Always start with an issue](https://about.gitlab.com/2016/03/03/start-with-an-issue/)
