# Versioning guidelines

We use semantic version through `npm`.

* `npm version patch`: For fixes/patches
* `npm version minor`: For new features
* `npm version major`: For breaking changes in the API

Or directly with `git tag vMAJOR.MINOR.PATCH`.

This tag is set on the `develop`- (if present) or on the `master`-[branch](BRANCHING.md).

## Further resources

* [Commit guidelines](COMMITS.md)
