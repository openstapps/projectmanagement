# Commit guidelines

## Template

Commit subjects should match the following template:

> `TYPE`: `SUBJECT`
> 
> `DESCRIPTION`

### `TYPE`

`TYPE` can have one of the following values:

| value | meaning |
| --- | --- |
| build | Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm) |
| ci | Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs) |
| docs | Documentation only changes |
| feat | A new feature |
| fix | A bug fix |
| perf | A code change that improves performance |
| refactor | A code change that neither fixes a bug nor adds a feature |
| style | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| test | Adding missing tests or correcting existing tests |

These are the [types](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#type), that angular uses. They are easily adaptable for all TypeScript based web related projects.

### `SUBJECT`

`SUBJECT` should be written in the imperative mood. You can think of a short subject that completes a sentence that starts with "This commit will...".

The first line of the commit subject should not be longer than 50 chars overall.

### `DESCRIPTION`

`DESCRIPTION` can contain a longer description of the commit. Each line should not be longer than 70 chars. New lines can be used to structure the description.

To reference or close [issues](ISSUES.md) GitLab [keywords](https://docs.gitlab.com/ce/user/project/issues/automatic_issue_closing.html) can be used in the last line(s) of the description:

> Fixes #42

or

> References #42

## Advantages

The result of those commit guidelines is a concise and clear history of the development.

Another advantage is the automatic [generation](https://www.npmjs.com/package/conventional-changelog-cli) of a changelog that gives an overview of the changes.

```shell
conventional-changelog -p angular -i CHANGELOG.md -s -r 0
```

## Further resources

* [Recommendations on commit messages](https://chris.beams.io/posts/git-commit/)
* [Versioning](VERSIONING.md)
