# Coding style

These guidelines are meant for TypeScript based project but can be adapted for projects that are developed in other languages as well.

## Configuration

Consistent code structure and style is ensured by using [`@openstapps/configuration`](https://gitlab.com/openstapps/configuration). It contains configuration files for your editor, the TypeScript compiler and TSLint.

When you install this project and are missing any of the recommended configuration files, they will be automatically created for you.

## Comments

Plenty of inline comments should be used to make it easier to understand what the code is doing.

Inline comments should not explain what code from external projects is doing. Add missing documentation to those external projects.

## Further resources

* [Commit guidelines](COMMITS.md)
* [TypeScript coding guidelines](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines)
* [Licensing guidelines](LICENSING.md)
