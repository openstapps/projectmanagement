# Publishing

## NPM

All projects that use the [OpenStApps configuration](https://gitlab.com/openstapps/configuration) and implement the automatic publishing via GitLab CI can simply be published to the NPM registry by using the appropriate job. It uses the `NPM_AUTH_TOKEN`  that is a [protected variable](https://gitlab.com/groups/openstapps/-/settings/ci_cd) of the OpenStApps group. To trigger this job, a new tag has to be created (on the master branch):

### Use current [master branch](BRANCHING.md)

```shell
git checkout master
git pull
```

### Update [version](VERSIONING.md)

```shell
npm version (major|minor|patch)
```

After these steps there should 2 new commits:

* A version commit
* A changelog commit

Also a new git tag should be created.

**CAUTION: Make sure that everything was successful, before running the next command, that pushes commits to GitLab!**

### Pushing commits and version tag created in previous step

To push the previously created commits and version tag you can use the following convenience script:

```shell
npm run push
```

After you've pushed the commits and tags a pipeline will be started for the tag that includes a job that will automatically publish a new package to the NPM registry.
