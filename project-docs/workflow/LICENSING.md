# Licensing guidelines

This and affiliated projects are licensed under GPLv3 or APGLv3, if not stated otherwise.

## Licensing projects

Copy the license from [projectmanagement](../../LICENSE) or the [official standalone](https://www.gnu.org/licenses/gpl-3.0-standalone.html) (with the filename `LICENSE`) into the root of your project.

## The GPL copyright notice

According to the GPLv3 guidelines for [adding the license to a new program](http://www.gnu.org/licenses/gpl-3.0-standalone.html#howto) a license note should be added at the beginning of each source file.

```typescript
/*
 * Copyright (C) <year> StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
```

### New Files

The `<year>` should be inserted with the current year. E.g.:

```typescript
 * Copyright (C) 2019 StApps
```

### Updating Files

If the file is updated in a new year after its creation, the `<year>` should be appended as a comma seperated list with spaces in between. E.g.:

```typescript
 * Copyright (C) 2018, 2019, 2021 StApps
```

## Further resources

* [Coding style](CODING.md)
