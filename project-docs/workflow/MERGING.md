# Merging & merge requests

Once development of an [issue](ISSUES.md) on a [branch](BRANCHING.md) is completed the merge request becomes relevant. It also can be used during development for continuous reviews and iterative improvements.

A merge request can also gather the work of multiple issues.

Merge requests are used to merge the changes to the main branches (`master`, `develop`). Every merge request needs to be reviewed/commented by at least 2 other developers before they can be accepted. This ensure that all guidelines are followed and that the scope of the issue is matched.

Before any merge of an `issue`-branch into the according `master`-branch, the commits of the `issue`-branch shall be tidied up.
Unstage your commits from the `issue`-branch (`git reset master`). Create your new commit(s) and push them to the remote repository (`git push --force`).  

## Further resources

* [Commits](COMMITS.md)
