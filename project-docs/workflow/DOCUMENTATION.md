# Documentation

Every projects need a `README.md` as a standardized entry of information about the scope of a [project](PROJECT.md) and how to install and execute it.

## Inline (code) documentation

Code is annotated with [TypeDoc](https://typedoc.org/), which then can be used to generate complete documentations of the API of the code.

### Guidelines

Please follow these guidelines to reduce redundance:

* The purpose of inline documentation is to explain what the line of code actually does without explaining syntax or external references. Example:

```typescript
// extend the template by the properties of the base template
templateBase.properties = mergeObjects(
  templateBase.properties,
  templates['base.template.json'].mappings._default_.properties
);
```

* Follow the [recommendations](http://typedoc.org/guides/doccomments/) of `typedoc`, because it is used to generate documentation. Do not denote the types again in the documentation (`@param` and `@returns`), because they are already "documented" in the code itself. Example:

```typescript
/**
 * Gets a value from the storage using the provided key
 *
 * @param key Unique identifier of the wanted resource in storage
 */
async get<T>(key: string): Promise<T> {
  const entry = await this.storage.get(key);
  if (!entry) {
    throw new Error('Value not found.');
  }
  return entry;
}
```

### Inline Comments `//`

* Start inline comments with a lowercase letter and a space after the `//`
* Place the comment above the line that it is referencing

```typescript
// lorem ipsum
const lorem;
```

### Doc Comments `/**`

* Start with a capital letter
* Keep the first line short
* The first line should not end with a period, nor should it consist of multiple sentences
* The first line should be easily scannable
* If you want to comment more than one line, do a short summary in the first line, then continue after a blank line with the more detailed description
* Document all parameters in functions using `@param`
* `@param` must not contain a type annotation, just `@param name description`
* Do not include `@return`, as it is redundant information

```typescript
/**
 * Short summary of the function, without a period
 *
 * This is a very long description, that could never possibly fit on one line, nor could it
 * be read in a short amount of time. Because we can use multiple sentences here, we can actually
 * use a period.
 *
 * @param foo This parameter does something
 */
function fun(foo: number): Foo {
```

## `CONTRIBUTING.md`

Every project can have a `CONTRIBUTING.md` which explains how to contribute to the project either just by issues or code contributions.

## Issue templates

The path `.gitlab/issue_templates` can contain markdown files which act as templates for issues. These are available as an options during issue creation and just have to be filled by the creator of the issue. They act as orientation which information need sto be provided.

## Further resources

* [GitLab issue templates](https://gitlab.com/help/user/project/description_templates.md)
