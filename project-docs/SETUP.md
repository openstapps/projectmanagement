# Basic setup for development

## Required tools/packages

* [SSH](https://docs.gitlab.com/ee/ssh/README.html)
* [Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
* [Docker](https://docs.gitlab.com/ee/user/project/container_registry.html) & Docker-Compose
* Node.js & NPM - be sure to install the "Fermium" (14.x) LTS version, might need a [PPA](https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions) or use NVM
* [NVM](https://github.com/creationix/nvm#installation) - if you want to use multiple different versions of node

Example for Debian based distributions:

```shell
apt install ssh git docker docker-compose nodejs
```

## IDE - recommended choices

* [VSCode](https://code.visualstudio.com/)
* [Webstorm](https://www.jetbrains.com/webstorm/download/) - Educational or OpenSource License **cannot** be used

## Optional helpful tools

* Curl - for executing HTTP requests on the command line
* Postman or Insomnia - for executing HTTP requests with a GUI

## Before you start (Windows only)

As we are working on LF line-endings only, make sure your editior or IDE does not introduce CRLF line-endings.
Depending on your preferred settings you could consider applying `git config core.eol lf` and `git config core.autocrlf input` within our reposiories.

## Clone starter repositories

* [Minimal deployment](https://gitlab.com/openstapps/minimal-deployment) - contains backend, database, minimal connector, copy (from api) and app
* [Minimal connector](https://gitlab.com/openstapps/minimal-connector) - an example connector to learn the principles of connector development

```shell
git clone git@gitlab.com:openstapps/minimal-deployment.git
git clone git@gitlab.com:openstapps/minimal-connector.git
```
